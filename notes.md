
New client reference
https://coderepo.carfax.net/projects/DVD/repos/carfax-connect-demo-tool/browse/front-end/src/components/config/PartnerAuth.ts#13

Auth Domains:
https://coderepo.carfax.net/projects/DVD/repos/carfax-connect-demo-tool/browse/front-end/src/utils/Constants.tsx

```
    public static readonly AUTH_DOMAIN: Map<string, string> = new Map([
        ['local', "staging-auth.carfax.com"],
        ['development', "staging-auth.carfax.com"],
        ['staging', "staging-auth.carfax.com"],
        ['production', "auth.carfax.com"]
    ])
```
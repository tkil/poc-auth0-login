const path = require("path");
const dotenv = require("dotenv");
const level = (process.env.LEVEL || process.env.PROMOTION_LEVEL || "LOCAL").toLocaleUpperCase();

let envFile = ".env.secrets";

const envPath = path.join(__dirname, envFile);
const config = () => dotenv.config({ path: envPath });

module.exports = {
  config,
  envPath,
  level,
};

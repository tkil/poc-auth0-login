import "regenerator-runtime/runtime.js";

import "./styles/__import.css";

import { Auth0Client } from "@auth0/auth0-spa-js";

const _DOMAIN = "staging-auth.carfax.com";
const _CLIENTID = process.env.AUTH0_CLIENT_ID;
const _REDIRECTURLS = {
  signin: "https://beta.carfax.com",
  signup: "https://beta.carfax.com/idk",
};
const _AUDIENCE = "https://www.carfax.com/api";

const auth0 = new Auth0Client({ domain: _DOMAIN, client_id: _CLIENTID, audience: _AUDIENCE });

async function login() {
  const signinUrl = _REDIRECTURLS && _REDIRECTURLS.signin ? _REDIRECTURLS.signin : undefined;
  const signupUrl = _REDIRECTURLS && _REDIRECTURLS.signup ? _REDIRECTURLS.signup : undefined;
  const redirect_uri = signinUrl ? signinUrl : signupUrl ? signupUrl : window.location.href;
  await auth0.loginWithRedirect({ redirect_uri });
}

// Assign to window to globalize
// @ts-ignore
window.login = login;
